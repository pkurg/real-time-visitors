<?php namespace Pkurg\Visitors\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Pkurg\Visitors\Models\VisitorDataModel as DataModel;
use Response;

class VisitorController extends Controller
{
	public $implement = [    ];
	
	public function __construct()
	{
		parent::__construct();
	}



	public function getdata()
	{
		

		$data =  DataModel::findOrCreate(1);

		$j = json_encode(['ip' => $data->ip, 'time' => $data->time, 'url' => $data->url, 'id' => $data->uniqid]);

		return Response::json(array( $j));
	}

}
