<?php namespace Pkurg\Visitors\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePkurgVisitorsData extends Migration
{
    public function up()
    {
        Schema::create('pkurg_visitors_data', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('url')->nullable();
            $table->string('uniqid')->nullable();
            $table->string('time')->nullable();
            $table->string('ip')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pkurg_visitors_data');
    }
}
