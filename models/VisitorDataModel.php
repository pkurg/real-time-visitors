<?php namespace Pkurg\Visitors\Models;

use Model;

/**
 * Model
 */
class VisitorDataModel extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pkurg_visitors_data';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public static function findOrCreate($id)
    {
        $obj = static::find($id);
        return $obj ?: new static;
    }

}
