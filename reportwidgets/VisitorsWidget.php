<?php namespace Pkurg\Visitors\ReportWidgets;

use Backend\Classes\ReportWidgetBase;
use Backend;
use Pkurg\Visitors\Models\VisitorDataModel as DataModel;

class VisitorsWidget extends ReportWidgetBase
{

	public function defineProperties()
	{
		return [
			'widget-title' => [
				'title' => 'Widget title',
				'default' => 'Real time visitors',
				'type' => 'string',
				'validationPattern' => '^.+$',
				'validationMessage' => 'The Widget Title is required.',
			],
			'widget-height' => [
				'title' => 'Widget height',				
				'default' => 180,
				'type' => 'string',
				'validationPattern' => '^[0-9]+$',
				'validationMessage' => 'The Max Items property can contain only numeric symbols',
			],
			'update-interval' => [
				'title' => 'Update interval',				
				'default' => 1000,
				'type' => 'string',
				'validationPattern' => '^[0-9]+$',
				'validationMessage' => 'Property can contain only numeric symbols',
			],
			'show-ip' => [
				'title' => 'Show ip adress',				
				'default' => 1,
				'type' => 'checkbox',				
			],
			'show-date' => [
				'title' => 'Show date',				
				'default' => 1,
				'type' => 'checkbox',				
			],

		];
	}

	public function render()
	{

		$data = DataModel::findOrCreate(1);
		$data->url = '';
		$data->ip = '';
		$data->time = '';
		$data->uniqid = 'none';
		$data->save();
	
		$this->vars['url'] = Backend::url('pkurg/visitors/visitorcontroller/getdata');

		return $this->makePartial('widget');

	}
}
