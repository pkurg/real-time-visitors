<?php namespace Pkurg\Visitors;

use Event;
use Pkurg\Visitors\Models\VisitorDataModel as DataModel;
use Request;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public function registerReportWidgets()
    {
        return [
            'Pkurg\Visitors\ReportWidgets\VisitorsWidget' => [
                'label' => 'Real time visitors',
                'context' => 'dashboard',
            ],
        ];
    }

    public function boot()
    {

        Event::listen('cms.page.display', function ($controller, $url, $page, $result) {

            $data = DataModel::findOrCreate(1);
            $data->url = url($url);
            $data->ip = Request::ip();
            $data->time = date('Y-m-d H:i:s');
            $data->uniqid = uniqid();
            $data->save();

        });

    }

}
